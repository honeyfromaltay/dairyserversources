process.env.NODE_ENV = 'test';
let app = require('../index');
let should = require('should');
let assert = require('assert');
describe('languageCode', () => {
    describe('ru', () => {
        it('ru-ru', function () {
            assert.equal('ru', app.getLangCode('ru-ru'));
        });
        it('ru-en', function () {
            assert.equal('ru', app.getLangCode('ru-en'));
        });
        it('ru', function () {
            assert.equal('ru', app.getLangCode('ru'));
        });
        it('en-ru', function () {
            assert.notEqual('ru', app.getLangCode('en-ru'));
        });
    });
    describe('en', () => {
        it('en-en', function () {
            assert.equal('en', app.getLangCode('en-en'));
        });
        it('en-ru', function () {
            assert.equal('en', app.getLangCode('en-ru'));
        });
        it('en', function () {
            assert.equal('en', app.getLangCode('en'));
        });
        it('ru-en', function () {
            assert.notEqual('en', app.getLangCode('ru-en'));
        });
    })
});

describe('jsDate', () => {
    describe('ruLocale', () => {
        it('date13-03-2012Ru', () => {
            assert.equal('2012-3-13', app.getJSDateByLanguageCode('13-03-2012', 'ru'));
        });
        it('date13.3.2012Ru', () => {
            assert.equal('2012-3-13', app.getJSDateByLanguageCode('13.3.2012', 'ru'));
        });
        it('date01.1+1956Ru', () => {
            assert.equal('1956-1-1', app.getJSDateByLanguageCode('01.1+1956', 'ru'));
        });
        it('date13.30.2012Ru', () => {
            assert.equal(null, app.getJSDateByLanguageCode('13.30.2012', 'ru'));
        });
        it('date50.2.2012Ru', () => {
            assert.equal(null, app.getJSDateByLanguageCode('50.2.2012', 'ru'));
        });
        it('date29.2.2020Ru', () => {
            assert.equal('2020-2-29', app.getJSDateByLanguageCode('29.2.2020', 'ru'));
        });
        it('date29.2.2019Ru', () => {
            assert.equal(null, app.getJSDateByLanguageCode('29.2.2019', 'ru'));
        });
    });
    describe('ebLocale', () => {
        it('date03-13-2012En', () => {
            assert.equal('2012-3-13', app.getJSDateByLanguageCode('03-13-2012', 'en'));
        });
        it('date3.13.2012En', () => {
            assert.equal('2012-3-13', app.getJSDateByLanguageCode('3.13.2012', 'en'));
        });
        it('date1.01+1956En', () => {
            assert.equal('1956-1-1', app.getJSDateByLanguageCode('1.01+1956', 'en'));
        });
        it('date30.13.2012En', () => {
            assert.equal(null, app.getJSDateByLanguageCode('30.13.2012', 'en'));
        });
        it('date2.50.2012En', () => {
            assert.equal(null, app.getJSDateByLanguageCode('2.50.2012', 'en'));
        });
        it('date2.29.2020En', () => {
            assert.equal('2020-2-29', app.getJSDateByLanguageCode('2.29.2020', 'en'));
        });
        it('date2.29.2019En', () => {
            assert.equal(null, app.getJSDateByLanguageCode('2.29.2019', 'en'));
        });
    })
});

describe('period_test', () => {
    describe('periodRu', () => {
        it('[01-02-2017]', () => {
            let arr = app.period_test('ru', ['01.02.2017'], 1);
            should(arr).be.a.Array;
            should(arr).be.length(1);
            should(arr[0]).be.String;
            should(arr[0]).be.equal('2017-2-1');
        });
        it('[01-02-2017, 04-10-2017]', () => {
            let arr = app.period_test('ru', ['01.02.2017', '04.10+2017'], 2);
            should(arr).be.a.Array;
            should(arr).be.length(2);
            should(arr[0]).be.String;
            should(arr[1]).be.String;
            should(arr[0]).be.equal('2017-2-1');
            should(arr[1]).be.equal('2017-10-4');
        });
    });
    describe('periodEn', () => {
        it('[02-01-2017]', () => {
            let arr = app.period_test('en', ['02.01.2017'], 1);
            should(arr).be.a.Array;
            should(arr).be.length(1);
            should(arr[0]).be.String;
            should(arr[0]).be.equal('2017-2-1');
        });
        it('[02-01-2017, 10-4-2017]', () => {
            let arr = app.period_test('en', ['02.01.2017', '10.4+2017'], 2);
            should(arr).be.a.Array;
            should(arr).be.length(2);
            should(arr[0]).be.String;
            should(arr[1]).be.String;
            should(arr[0]).be.equal('2017-2-1');
            should(arr[1]).be.equal('2017-10-4');
        });
    });
});

describe('nsDateFormatterForInlineRows', () => {
    describe('dateNoteRu', () => {
        it('ru, now ', () => {
            let nowDate = new Date();
            let seconds = nowDate.getSeconds();
            let minutes = nowDate.getMinutes();
            let hours = nowDate.getHours();
            let date = nowDate.getDate();
            let month = nowDate.getMonth();
            let year = nowDate.getFullYear();
            let nsDate = app.nsDateFormatterForInlineRows('ru', nowDate);
            should(nsDate).be.a.Object;
            should(nsDate.title).be.a.String;
            should(nsDate.text).be.a.String;
            should(nsDate.title).be.equal(hours + ":" + minutes);
            should(nsDate.text).be.equal('<b>' + date +
                '-' + (Number(month) + 1).toString() +
                '-' + year +
                ' ' + hours + ':' + minutes + ':' + seconds +
                '</b>');
        });
        it('ru, now minus one day', () => {
            let nowDate = new Date();
            nowDate.setDate(nowDate.getDate() - 1);
            let seconds = nowDate.getSeconds();
            let minutes = nowDate.getMinutes();
            let hours = nowDate.getHours();
            let date = nowDate.getDate();
            let month = nowDate.getMonth();
            let year = nowDate.getFullYear();
            let nsDate = app.nsDateFormatterForInlineRows('ru', nowDate);
            should(nsDate).be.a.Object;
            should(nsDate.title).be.a.String;
            should(nsDate.text).be.a.String;
            should(nsDate.text).be.equal('<b>' + date +
                '-' + (Number(month) + 1).toString() +
                '-' + year +
                ' ' + hours + ':' + minutes + ':' + seconds +
                '</b>');
        });
    });
    describe('dateNoteEn', () => {
        it('en, now ', () => {
            let nowDate = new Date();
            let seconds = nowDate.getSeconds();
            let minutes = nowDate.getMinutes();
            let hours = nowDate.getHours();
            let date = nowDate.getDate();
            let month = nowDate.getMonth();
            let year = nowDate.getFullYear();
            let nsDate = app.nsDateFormatterForInlineRows('en', nowDate);
            should(nsDate).be.a.Object;
            should(nsDate.title).be.a.String;
            should(nsDate.text).be.a.String;
            should(nsDate.title).be.equal(hours + ":" + minutes);
            should(nsDate.text).be.equal('<b>' + (Number(month) + 1).toString() +
                '-' + date +
                '-' + year +
                ' ' + hours + ':' + minutes + ':' + seconds +
                '</b>');
        });
        it('en, now minus one day', () => {
            let nowDate = new Date();
            nowDate.setDate(nowDate.getDate() - 1);
            let seconds = nowDate.getSeconds();
            let minutes = nowDate.getMinutes();
            let hours = nowDate.getHours();
            let date = nowDate.getDate();
            let month = nowDate.getMonth();
            let year = nowDate.getFullYear();
            let nsDate = app.nsDateFormatterForInlineRows('en', nowDate);
            should(nsDate).be.a.Object;
            should(nsDate.title).be.a.String;
            should(nsDate.text).be.a.String;
            should(nsDate.text).be.equal('<b>' + (Number(month) + 1).toString() +
                '-' + date +
                '-' + year +
                ' ' + hours + ':' + minutes + ':' + seconds +
                '</b>');
        });
    });
});
// describe('Array', function () {
//     describe('#indexOf()', function () {
//         it('should return -1 when the value is not present', function () {
//             assert.equal(-1, [1, 2, 3].indexOf(4));
//         });
//     });
// });