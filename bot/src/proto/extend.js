Date.prototype.getUnixTime = function () {
    return this.getTime() / 1000 | 0;
};
Date.prototype.getDateTime = function () {
    return this.toLocaleString("ru");
};
