let dictionary = {};

dictionary.HelpText = "Чтобы запомнить вашу мысль просто введите ваш текст в окно сообщения. \n" +
    "Чтобы вспомнить ваши мысли - напишите /н  или /найти и перечислите ключевые слова вашей мысли \n" +
    "Чтобы вывести мысли за период - напишите /п или /период от [до] \n" +
    "Чтобы вывести подсказку - напишите /помощь\n";

dictionary.SphinxBadRequest = "Что-то пошло не так! Попробуйте поменять запрос.\n" +
    "Вы можете набрать /период!";

dictionary.MySqlErrorFrom = "Плохие новости. Всё сломано!";

dictionary.SphinxFoundNothing = "Ничего не найдено!";

dictionary.PeriodHelpText = "Варианты использования /период:\n" +
    "/период [Начиная] [До], где Начиная и До - даты в формате День-Месяц-Год\n" +
    "/период От, где От - Дата в формате День-Месяц-Год\n" +
    "/период - Подсказка\n" +
    "Примеры:\n" +
    "1) /период 01-02-2017 01-05-2017 - Выдать все записи с 1 февраля 2017 года" +
    " по 1 мая 2017 года\n" +
    "2) /период 01-01-2017 - Выдать все записи от 1 января 2017 года";

dictionary.PeriodProblemWithDateFrom = "Не правельный формат даты [Начиная с]\n" +
    "Должно быть ДД-ММ-ГГГГ";

dictionary.PeriodProblemWithDateTo = "Не правельный формат даты [Заканчивая]\n" +
    "Должно быть ДД-ММ-ГГГГ";



dictionary.About = "Design Andrew Romanov scalli-k-be@ya.ru \n" +
    "Programming Ivan Dolgov shmuft@gmail.com\n" +
    "License THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS" +
    " \"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED " +
    "TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR " +
    "PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS " +
    "BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, " +
    "OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF " +
    "SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS " +
    "INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN " +
    "CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) " +
    "ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE " +
    "POSSIBILITY OF SUCH DAMAGE.";

dictionary.AddedNewNote = "Запись добавлена!";

dictionary.SearchHelp = 'Может быть вы имеете в виду \n/найти слово слово\n' +
    'или /найти "Фраза" "Фраза"\n' +
    'или /найти Слово "Фраза"?';

dictionary.BadNextMessage = "Закончились следующие сообщения!";

dictionary.BadCommand = "Не верный формат команды!";
dictionary.NotSupportedMessageType = "Не поддерживаемый тип данных для сохранения в ежедневник.";

//List of commands

module.exports.dictionary = dictionary;