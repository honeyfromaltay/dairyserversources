let dictionary = {};
dictionary.HelpText = "To save your idea just enter your text in the message box. \n" +
    "To remember your thoughts - write /s and list of the key words of your thoughts \n" +
    "To see the thoughts for a period - write '/p or /period from [to]' \n" +
    "To give help, write /help\n";

dictionary.SphinxBadRequest = "Something wrong! May be you need to change your query?\n" +
    "Try /period in your console!";

dictionary.MySqlErrorFrom = "Bad news. Something wrong! We'll fix it.";

dictionary.SphinxFoundNothing = "Found nothing.";

dictionary.PeriodProblemWithDateFrom = "It is bad date format [From]\n" +
    "Must be MM-DD-YYYY";

dictionary.PeriodProblemWithDateTo = "It is bad date format [To]\n" +
    "Must be MM-DD-YYYY";

dictionary.PeriodHelpText = "Usage of /period:\n" +
    "/period From To, where From and To - date in the format Year-Month-Day\n" +
    "/period To, where To - date in the format Year-Month-Day\n" +
    "/period - the Tooltip\n" +
    "Examples:\n" +
    "1) /period 01-01-2017 05-01-2017 - to Give all records from January 1, 2017," +
    "on may 1, 2017\n" +
    "2) /period 05-01-2017 - to Give all the records after January 1, 2017";

dictionary.About = "Design Andrew Romanov scalli-k-be@ya.ru \n" +
    "Programming Ivan Dolgov shmuft@gmail.com";

dictionary.AddedNewNote = "Note added.";
dictionary.BadNextMessage = "No next note!";
dictionary.BadCommand = "Bad command format!";
dictionary.NotSupportedMessageType = "Not supported message type";
module.exports.dictionary = dictionary;
