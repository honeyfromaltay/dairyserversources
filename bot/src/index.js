console.log("Started!");
require("./proto/extend");
let errors = require("./errors").errors;
let config = require("./libs/config").Nconf;
const Telegraf = require("telegraf");
//const app = new Telegraf(config.get("telegrafKey"));
const app = new Telegraf(process.env.TELEGRAM_TOKEN);
const ruDict = require("./lang/ru").dictionary;
const enDict = require("./lang/en").dictionary;
let logger = require("./libs/logger").logger;
let NoteModel = require('./libs/mysqldb').NoteModel;
let NoteSphinxModel = require('./libs/sphinxdb').NoteSphinxModel;
const Markup = require('telegraf/markup');
let Dict = {ru: ruDict, en: enDict};

function getLangCode(language_code) {
    let result = language_code.match(/(.{2})(.*)/);
    switch (result[1]) {
        case "ru":
            return "ru";
        case "en":
        default:
            return "en";
    }

}
console.log("Bot started");
/**
 *
 * @param {Array} rows
 * @param rows.date_added
 * @param language_code
 * @returns {string}
 */
function printNotes(rows, language_code) {
    let backText = "";
    rows.forEach(function (note) {
        let noteDate = new Date(note.date_added);
        switch (language_code) {
            case "ru":
                backText += ( "<b>" + noteDate.getDate() + "-" + (Number(noteDate.getMonth()) + 1).toString() + "-" +
                    noteDate.getFullYear() + " " + noteDate.getHours() + ":" +
                    noteDate.getMinutes() + ":" + noteDate.getSeconds() + "</b>\n"
                    + note.note + "\n\n");
                break;
            default:
                backText += ( "<b>" + (Number(noteDate.getMonth()) + 1).toString() + "-" + noteDate.getDate() + "-" +
                    noteDate.getFullYear() + " " + noteDate.getHours() + ":" +
                    noteDate.getMinutes() + ":" + noteDate.getSeconds() + "</b>\n"
                    + note.note + "\n\n");
        }
    });
    return backText;
}


// app.use((ctx, next) => {
//     console.log(ctx);
//     next();
// });

/*
* You need some help?
*/
app.hears(/^\/(help|помощь)/, (ctx) => {
    ctx.reply(Dict[getLangCode(ctx.from.language_code)].HelpText);
});

app.hears(/^\/(start|старт) (.+)/, (ctx) => {
    //Добавляем нового пользователя в таблицу users.
    //Потом поприветствуем его. Проверим так же, есть ли он уже или нет?

    NoteModel.RegisterNewUser(ctx.from.id, ctx.from.first_name + " " + ctx.from.last_name, function (err, req) {
        if (err) {
            logger.log(err);
        }
        if (ctx.match[2]) {
            let buttons = [];
            // ctx.replyWithHTML(
            //     "<b>back text</b>",
            //     Markup
            //         .inlineKeyboard([
            //             Markup.switchToChatButton('Apply theme'),
            //         ])
            //         .removeKeyboard()
            //         .extra()
            // );
            buttons.push(Markup.callbackButton("Button", "test"));
            buttons.push(Markup.switchToChatButton("Back ", "ok"));
            // buttons.push(Markup.switchToChatButton("Back ", ""));
            ctx.reply("back text", Markup
                .inlineKeyboard(buttons)
                .removeKeyboard()
                .extra());
        } else {
            ctx.reply(Dict[getLangCode(ctx.from.language_code)].HelpText);
        }
    });
});

/**
 * Day must be from 1 to 31, 30, 28, 29
 * @param {Number} day
 * @param {Number} month
 * @param {Number} year
 * @returns {Boolean} - It is ok
 */
function isCorrectDay(day, month, year) {
    if (day < 1 || day > 31 || month < 0 || month > 11) {
        return false;
    }

    if (month === 1 && day > 28) {
        let x = (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0);
        if ((!x && day >= 29)) {
            return false;
        }
    }

    return true;
}

/**
 * return a Date in JS Style
 * @param {String} localDate - It is a date, which user send at console for the bot in a locale format.
 * @param {String} language_code - A language code (ru, en, ...)
 * @returns {String|null} - Compiled date in JS style
 */
function getJSDateByLanguageCode(localDate, language_code) {
    let dateArr = localDate.split(/(\d\d|\d).(\d\d|\d).(\d\d\d\d)/);
    if (dateArr.length === 0) {
        return null
    }
    let dateInLocal;
    switch (language_code) {
        case "ru":
            if (!isCorrectDay(Number(dateArr[1]), Number(dateArr[2]) - 1, Number(dateArr[3]))) {
                return null;
            }
            dateInLocal = new Date(Number(dateArr[3]), Number(dateArr[2]) - 1, Number(dateArr[1]));
            break;
        case "en":
        default:
            if (!isCorrectDay(Number(dateArr[2]), Number(dateArr[1]) - 1, Number(dateArr[3]))) {
                return null;
            }
            dateInLocal = new Date(Number(dateArr[3]), Number(dateArr[1]) - 1, Number(dateArr[2]));
    }

    if (isNaN(dateInLocal)) {
        return null;
    }

    return dateInLocal.getFullYear() + "-" + (Number(dateInLocal.getMonth()) + 1).toString()
        + "-" + dateInLocal.getDate();
}

/*
*Telegram bot command:
/period dateFrom dateTo,
date must have format Date() js
*/


/**
 * Compile dates into array with check on correct data type
 * @param {String} language_code - May be ru|en|...
 * @param {String[]} dataReg - DateFrom, DateTo in string
 * @param {Number} numberParameters - One or two dates
 * @returns {[Date, Date]|null} - [Date from, Date to]
 */
function period_test(language_code, dataReg, numberParameters) {
    let query = [];

    let from = getJSDateByLanguageCode(dataReg[0], language_code);
    if (from === null) {
        logger.log(new Error(Dict[language_code].PeriodProblemWithDateFrom + " "
            + from, errors.DateBadFormat));
        return null;
    }

    query.push(from);
    if (numberParameters === 2) {
        let to = getJSDateByLanguageCode(dataReg[1], language_code);

        if (to === null) {
            logger.log(new Error(Dict[language_code].PeriodProblemWithDateTo + " "
                + to, errors.DateBadFormat));
            return null;
        }
        query.push(to);
    }
    return query;
}


app.hears(/^\/(period|p|период|п) (.+) (.+)/, (ctx) => {
    let query = [ctx.match[2], ctx.match[3]];
    let language_code = getLangCode(ctx.from.language_code);
    period_test(language_code, query, 2)
        .then((data) => {
            generateReplySearch(language_code, data, 0, "period", false);
        })
        .catch((err) => {
            logger.log(err);
            ctx.reply(err);
        });
});

/*
Telegram bot command:
/period dateTo,
date must have format Date() js
*/

app.hears(/^\/(period|p|период|п) (.+)/, (ctx) => {
    let language_code = getLangCode(ctx.from.language_code);
    let periods = period_test(language_code, [ctx.match[2]], 1);
    generateReplySearch(ctx, periods, 0, "period", false);
});

/*
* Show help information About /period command
*/
app.hears(/^\/(period|p|период|п)/, (ctx) => {
    ctx.reply(Dict[getLangCode(ctx.from.language_code)].PeriodHelpText);
});

/*
* Find some text by key words
*/


/*
* Realised commands:
* search
* period
*/

/**
 *
 * @param ctx
 * @param ctx.from.language_code
 * @param {function} ctx.reply
 * @param {function} ctx.replyWithHTML
 * @param {function} ctx.editMessageReplyMarkup
 * @param searchQuery
 * @param groupNumber
 * @param command
 * @param isCallBack
 */
function generateReplySearch(ctx, searchQuery, groupNumber, command, isCallBack) {
    let language_code = getLangCode(ctx.from.language_code);
    NoteSphinxModel.NumberOfIds(ctx.from.id, searchQuery, command, function (err, numberIds) {
        NoteSphinxModel.FindIdsSearchQuery(ctx.from.id, searchQuery, command, groupNumber, function (err, ids) {
            if (err) {
                logger.log(err);
                ctx.reply(Dict[language_code].SphinxBadRequest);
                return;
            }
            if (ids.length === 0) {
                ctx.reply(Dict[language_code].SphinxFoundNothing);
                return;
            }

            NoteModel.FindNotesByTelegramId(ctx.from.id, ids, function (err, rows) {
                if (err) {
                    logger.log(err);
                    ctx.reply(Dict[language_code].MySqlErrorFrom);
                    return;
                }
                let message = "";
                message += printNotes(rows, language_code);
                let buttons = [];
                let modIds = numberIds / config.get("numberNotesInGroup");
                if (Number(groupNumber) + 1 < modIds && numberIds > config.get("numberNotesInGroup")) {
                    if (language_code === "ru") {
                        buttons.push(Markup.callbackButton("Ещё",
                            "but" + (Number(groupNumber) + 1).toString() + " " + command + " " + searchQuery));

                        message += "<b>Осталось ещё " +
                            (Number(numberIds) - (Math.floor(groupNumber) + 1) * config.get("numberNotesInGroup")).toString() +
                            " записей из " + numberIds.toString() + "</b>\n";
                    } else {
                        buttons.push(Markup.callbackButton("next",
                            "but" + (Number(groupNumber) + 1).toString() + " " + command + " " + searchQuery));

                        message += "<b>There are " +
                            (Number(numberIds) - (Math.floor(groupNumber) + 1) * config.get("numberNotesInGroup")).toString() +
                            " more notes out of " + numberIds.toString() + "</b>\n";
                    }
                }

                if (numberIds <= config.get("numberNotesInGroup")) {
                    buttons = [];
                }
                // else {
                //     ctx.editMessageText(message + "-");
                //     ctx.editMessageReplyMarkup(Markup.inlineKeyboard(buttons));
                //     return ctx;
                // }
                // for (let i = 0; i < modIds; i++) {
                //     if (i == groupNumber) {
                //         buttons.push(Markup.callbackButton(">"+i.toString()+"<", "but" + i.toString() + " " + keywords));
                //     } else {
                //         buttons.push(Markup.callbackButton(i.toString(), "but" + i.toString() + " " + keywords));
                //     }
                // }
                NoteModel.UpdateOrCreateSessionKeywords(ctx.from.id, searchQuery, command, groupNumber, function (err) {
                    if (err) {
                        logger.log(err);
                        return
                    }

                    if (!isCallBack) {
                        ctx.replyWithHTML(message, Markup.inlineKeyboard(buttons).extra());
                    }
                    else {
                        //Нужно делать с перерывом, так как телеграм бот имеет ограничение 0,3 секунды между
                        //сообщениями. Иначе бан на несколько минут!
                        ctx.editMessageReplyMarkup(Markup.inlineKeyboard([]).extra());
                        setTimeout(function () {
                            ctx.replyWithHTML(message, Markup.inlineKeyboard(buttons).extra());
                        }, 1010);
                    }
                });

            });
        });
    });
}


app.hears(/^\/(search|s|найти|н") (.+)/, (ctx) => {
    generateReplySearch(ctx, ctx.match[2], 0, "search", false);
});

app.hears(/^\/(next|n|далее|д)/, (ctx) => {
    NoteModel.FindLastNext(ctx.from.id, function (err, rows) {
        if (rows.length === 0) {
            ctx.reply(Dict[getLangCode(ctx.from.language_code)].BadNextMessage);
            return;
        }

        generateReplySearch(ctx, rows[0].searchQuery.split(","), rows[0].groupNumber + 1, rows[0].command, false);
    })
});

app.action(/^but(.+) (.+) (.+)/, (ctx) => {
    switch (ctx.match[2]) {
        case "search":
        case "найти":
            generateReplySearch(ctx, ctx.match[3], ctx.match[1], "search", true);
            break;
        case "period":
        case "период":
            generateReplySearch(ctx, ctx.match[3].split(","), ctx.match[1], "period", true);
            break;
    }
});

app.action(/^\/(search|s|найти|н)/, (ctx) => {
    ctx.reply(Dict[getLangCode(ctx.from.language_code)].SearchHelp);
});

/*
* About command
* Display Moo information
*/
app.action(/^\/(about|о)/, (ctx) => {
    ctx.reply(Dict[getLangCode(ctx.from.language_code)].About);
});

app.hears(/^\/(.+)/, (ctx) => {
    let language_code = getLangCode(ctx.from.language_code);
    ctx.reply(Dict[language_code].BadCommand + "\n" + Dict[language_code].HelpText);
});

/**
 * Добавляем запись в базу данных и в индексирование Сфинкса.
 * @param {Object} ctx - Контекст, может быть от разных режимов бота.
 * @param {Function} callback - Функция вывода сообщения для пользователя
 */
function addNote(ctx, callback) {
    let note = new NoteModel({
        telegram_id: ctx.from.id,
        note: ctx.message.text
    });
    NoteModel.AddNoteIntoDatabase(note, function (err, data) {
        if (err) {
            logger.log(err);
            callback(Dict[getLangCode(ctx.from.language_code)].MySqlErrorFrom);
            return;
        }
        NoteSphinxModel.AddInsertedIdAndDataIntoRTIndex({
            data: note,
            insertId: data.insertId
        }, function (err, data) {
            if (err) {
                logger.log(err);
                callback(Dict[getLangCode(ctx.from.language_code)].MySqlErrorFrom);
                return;
            }
            callback(Dict[getLangCode(ctx.from.language_code)].AddedNewNote);
        });
    });
}

/**
 * Слушаем на событие message.
 * Добавляем в базу данных сообщение по telegramId
 */

app.on("message", (ctx) => {
    //Делаем проверку, чтобы был только тип сообщения и подтип был один. Чтобы исключить запись гифок, видео и т.д.
    if (ctx.updateType === "message"
        && ctx.updateSubTypes.length === 1
        && ctx.updateSubTypes.indexOf("text") > -1) {
        addNote(ctx, function (message) {
            ctx.reply(message);
        });
    } else {
        ctx.reply(Dict[getLangCode(ctx.from.language_code)].NotSupportedMessageType);
    }
});

app.on("edited_message", (ctx) => {
    let reg = /^\/(search|s|найти|н|period|p|период|п) (.+)/i;
    let arr = ctx.editedMessage.text.match(reg);
    if (!Array.isArray(arr)) {
        ctx.replyWithHTML(Dict[getLangCode(ctx.from.language_code)].HelpText);
        return;
    }
    let language_code = getLangCode(ctx.from.language_code);
    let command = arr[1];
    let data = arr[2];
    switch (command) {
        case "search":
        case "s":
        case "найти":
        case "н":
            generateReplySearch(ctx, data, 0, "search", false);
            break;
        case "period":
        case "p":
        case "период":
        case "п":
            let reg2 = new RegExp(/(.+) (.+)/);
            let periods = data.match(reg2);

            let arrayOfPeriodsForSearch;
            if (Array.isArray(periods)) {
                arrayOfPeriodsForSearch = period_test(language_code, [periods[1], periods[2]], 2);
            } else {
                arrayOfPeriodsForSearch = period_test(language_code, [data], 1);
            }

            if (arrayOfPeriodsForSearch === null) {
                return;
            }
            generateReplySearch(ctx, arrayOfPeriodsForSearch, 0, "period", false);
            break;
        default:
            ctx.replyWithHTML(Dict[getLangCode(ctx.from.language_code)].HelpText);
    }
});

/**
 * В инлайн запросе из-за ограничения длины нужно формировать ответ в укороченном варианте даты,
 * если запись произведена в тот же день
 * @param {String} language_code - локаль пользователя
 * @param {Date} noteDate - дата записи
 * @returns {Object} nsDateObject
 * @returns {Object} nsDateObject.title
 * @returns {Object} nsDateObject.text
 */
function nsDateFormatterForInlineRows(language_code, noteDate) {
    let now = new Date();
    let backTitle = "";
    let backText = "";
    switch (language_code) {
        case "ru":
            if (noteDate.getFullYear() === now.getFullYear()
                && noteDate.getMonth() === now.getMonth()
                && noteDate.getDate() === now.getDate()) {

                backTitle += noteDate.getHours() + ":" + noteDate.getMinutes();
                backText += "<b>" + noteDate.getDate() + "-" + (Number(noteDate.getMonth()) + 1).toString() + "-" +
                    noteDate.getFullYear() + " " + noteDate.getHours() + ":" +
                    noteDate.getMinutes() + ":" + noteDate.getSeconds() + "</b>";
                break;
            }

            backTitle += noteDate.getDate() + "-" + (Number(noteDate.getMonth()) + 1).toString() + "-" +
                noteDate.getFullYear().toString().substring(-2);
            backText += "<b>" + noteDate.getDate() + "-" + (Number(noteDate.getMonth()) + 1).toString() + "-" +
                noteDate.getFullYear() + " " + noteDate.getHours() + ":" +
                noteDate.getMinutes() + ":" + noteDate.getSeconds() + "</b>";
            break;
        default:
            if (noteDate.getFullYear() === now.getFullYear()
                && noteDate.getMonth() === now.getMonth()
                && noteDate.getDate() === now.getDate()) {

                backTitle += noteDate.getHours() + ":" + noteDate.getMinutes();
                backText += "<b>" + (Number(noteDate.getMonth()) + 1).toString() + "-" + noteDate.getDate() + "-" +
                    noteDate.getFullYear() + " " + noteDate.getHours() + ":" +
                    noteDate.getMinutes() + ":" + noteDate.getSeconds() + "</b>";
                break;
            }
            backTitle += (Number(noteDate.getMonth()) + 1).toString() + "-" + noteDate.getDate() + "-" +
                noteDate.getFullYear().toString().substring(-2);
            backText += "<b>" + (Number(noteDate.getMonth()) + 1).toString() + "-" + noteDate.getDate() + "-" +
                noteDate.getFullYear() + " " + noteDate.getHours() + ":" +
                noteDate.getMinutes() + ":" + noteDate.getSeconds() + "</b>";
    }
    return {title : backTitle, text: backText};
}

/**
 *Компилируем объекты типа InlineQueryResultArticle в массив для отправки пользователю в инлайн режиме бота
 * @param {Array} rows - Записи пользователя из БД
 * @param {String} language_code - Локаль пользователя
 * @returns {Array} InlineQueryResult - В формате telegrambot api
 */
function compileInlineRows(rows, language_code) {
    let InlineQueryResult = [];
    rows.forEach(function (note) {
        let noteDate = new Date(note.date_added);
        let nsDateObject = nsDateFormatterForInlineRows(language_code, noteDate);
        let InlineQueryResultArticle = {
            type: 'article',
            id: (note.id).toString(),
            title: (nsDateObject.title + " " + note.note).substring(0, 40),
            input_message_content: {
                message_text: nsDateObject.text + "\n" + note.note + "\n\n",
                parse_mode: "HTML"
            },
        };
        InlineQueryResultArticle.push(InlineQueryResultArticle);

    });

    return InlineQueryResult;
}

/**
 * Функция вызывется при ответе на inline режим бота. @bot ......
 * @param {Object} ctx          - Контекст телеграм бота
 * @param {String} searchQuery  - Поисковый запрос
 * @param {String} command      - Команда (Найти или период)
 * @param {Number} offset       - Сдвиг поиска, нулевая группа из пяти, первая группа из следующих пяти
 * @param {Function} callback    - Callback функция
 */
function inlineAnswer(ctx, searchQuery, command, offset, callback) {
    let language_code = getLangCode(ctx.from.language_code);

    NoteSphinxModel.NumberOfIds(ctx.from.id, searchQuery, command, function (err, numberIds) {
        if (numberIds === 0) {
            callback(err, null);
            return;
        }
        NoteSphinxModel.FindIdsSearchQuery(ctx.from.id, searchQuery, command, offset, function (err, ids) {
            if (err) {
                logger.log(err);
                return null;
            }
            if (ids.length === 0) {
                return null;
            }

            NoteModel.FindNotesByTelegramId(ctx.from.id, ids, function (err, rows) {
                if (err) {
                    logger.log(err);
                    return null;
                }
                let message = compileInlineRows(rows, language_code);
                return callback(null, message);
            });
        });
    });
}

/**
 * Ответ на inline режим бота.
 */
id = 1234;
app.on("inline_query", async ({inlineQuery, answerInlineQuery}) => {
    let offset = parseInt(inlineQuery.offset) || 0;
    let reg = /^\/(search|s|найти|н|period|p|период|п) (.+)/i;
    let arr = inlineQuery.query.match(reg);
    if (!Array.isArray(arr)) {
        if (inlineQuery.query !== "") {
            let InlineQueryResultArticle = {
                type: 'article',
                title: "Не жать",
                id: "gotobot" + String(id),
                input_message_content: {
                    message_text: "Не жать",
                    parse_mode: "HTML"
                }
            };
            let options = {
                switch_pm_text: "Добавить запись в бота",
                switch_pm_parameter: inlineQuery.query,
                cache_time: 0,
                is_personal: true,
            };
            id++;
            setTimeout(function () {
                answerInlineQuery([InlineQueryResultArticle], options);
            }, 1010);
        }
        // answerInlineQuery.replyWithHTML(Dict[getLangCode(inlineQuery.from.language_code)].HelpText);
        return;
    }
    let command = arr[1];
    let data = arr[2];
    let language_code = getLangCode(inlineQuery.from.language_code);
    switch (command) {
        case "search":
        case "s":
        case "найти":
        case "н":
            inlineAnswer(inlineQuery, data, "search", offset, function (err, message) {
                if (err) {
                    logger.log(err);
                    return;
                }

                let options = {
                    next_offset: offset + 1,
                    is_personal: true,
                    cache_time: 0,
                };
                setTimeout(function () {
                    answerInlineQuery(message, options);
                }, 1010);
            });
            break;
        case "period":
        case "p":
        case "период":
        case "п":
            let regExpPeriod = new RegExp(/(.+) (.+)/);
            let periods = data.match(regExpPeriod);

            let arrayOfPeriodsForSearch;
            if (Array.isArray(periods)) {
                arrayOfPeriodsForSearch = period_test(language_code, [periods[1], periods[2]], 2);
            } else {
                arrayOfPeriodsForSearch = period_test(language_code, [data], 1);
            }

            if (arrayOfPeriodsForSearch === null) {
                return;
            }

            inlineAnswer(inlineQuery, arrayOfPeriodsForSearch, "period", offset, function (err, message) {
                if (err) {
                    logger.log(err);
                    return
                }

                let options = {
                    next_offset: offset + 1,
                    is_personal: true,
                    cache_time: 0,
                };
                setTimeout(function () {
                    answerInlineQuery(message, options);
                }, 1010);
            });
            break;
        default:
            answerInlineQuery("Ы");
    }
});

app.on("error", function (err) {
    logger.log(err);
});

app.startPolling();

module.exports = {getLangCode, getJSDateByLanguageCode, period_test, nsDateFormatterForInlineRows};
