var mongoose = require('mongoose');

var config = require('./config');

mongoose.connect('mongodb://localhost/test1', {
    useMongoClient: true
});
console.log(config.get('mongoose:uri'));

var db = mongoose.connection;

db.on('error', function (err) {
    console.error('connection error:', err.message);
});
db.once('open', function callback() {
    console.info('connected to db!');
});

var Schema = mongoose.Schema;

var Note = new Schema({
    login: {type: String, required: true},
    text: {type: String, required: true}
});

var NoteModel = mongoose.model('Note', Note);

module.exports.NoteModel = NoteModel;