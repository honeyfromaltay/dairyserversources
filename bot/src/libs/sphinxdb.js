let mysql = require('mysql');
let config = require('./config').Nconf;
let pool = mysql.createPool({
    connectionLimit: 3,
    host: config.get("hostSphinx"),
    port: "9306",
    debug: false
});
pool.on('acquire', function (connection) {
    if (config.get("debug_mode") === true) {
        console.log("Sphinx connection " + connection.threadId.toString() + "acqired");
    }
});

pool.on('release', function (connection) {
    if (config.get("debug_mode") === true) {
        console.log("Sphinx connection " + connection.threadId.toString() + "released");
    }
});

let errors = require("../errors").errors;
let table = config.get("sphinxIndexNotes");

function NoteSphinxModel() {
}

NoteSphinxModel.FindIdsSearchQuery = FindIdsBySearchQuery;
NoteSphinxModel.FindIdsByFromTo = FindIdsByFromTo;
NoteSphinxModel.FindIdsByFrom = FindIdsByFrom;
NoteSphinxModel.NumberOfIds = getNumberOfIds;
NoteSphinxModel.AddInsertedIdAndDataIntoRTIndex = addInsertedIdAndDataIntoRTIndex;

function getNumberIdsFromDatabase(sql, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            callback(err, null);
            return;
        }

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                callback(err, null);
                return;
            }
            callback(null, rows[0].cou);
        });
    });


}

function selectIdsFromDatabase(sql, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            callback(err, null);
            return;
        }

        connection.query(sql, function (err, rows) {
            connection.release();
            if (err) {
                callback(err, null);
                return;
            }
            let ids = [];
            rows.forEach(function (noteSphinx) {
                ids.push(noteSphinx.id);
            });
            callback(null, ids);
        });


        // connection.on('error', function (err) {
        //     connection.release();
        //     callback(err, null);
        // });


    });

}

function getNumberOfIds(telegram_id, searchQuery, command, callback) {
    let sqlSelect = "";
    switch (command) {
        case "search":
            sqlSelect = "SELECT count(*) as cou FROM " + table + " WHERE telegram_id=" + telegram_id.toString() +
                " AND MATCH('" + searchQuery + "') ORDER BY date_added ASC";
            getNumberIdsFromDatabase(sqlSelect, callback);
            break;
        case "period":
            sqlSelect = "SELECT count(*) as cou FROM " + table;
            try {
                if (searchQuery.length == 1) {
                    FindIdsByFrom(telegram_id, sqlSelect, "", searchQuery[0], getNumberIdsFromDatabase, callback);
                } else {
                    FindIdsByFromTo(telegram_id, sqlSelect, "", searchQuery[0], searchQuery[1], getNumberIdsFromDatabase, callback);
                }
            } catch (err) {
                // console.log(searchQuery);
                // console.log(err);
            }
            break;
    }

}

/*
* if command is search then searchQuery may be only keywords,
* if command is period then searchQuery is array [from] or [from, to]
*/

function FindIdsBySearchQuery(telegram_id, searchQuery, command, gr, callback) {
    let numberNotesInGroup = config.get("numberNotesInGroup");
    let sqlSelect = "";
    switch (command) {
        case "search":
            sqlSelect = "SELECT id FROM " + table + " WHERE telegram_id=" + telegram_id.toString() +
                " AND MATCH('" + searchQuery + "') ORDER BY date_added ASC LIMIT "
                + (Number(numberNotesInGroup) * gr).toString() + ", " + numberNotesInGroup;
            selectIdsFromDatabase(sqlSelect, callback);
            break;
        case "period":
            sqlSelect = "SELECT * FROM " + table;
            let sqlEnd = " LIMIT " + (Number(numberNotesInGroup) * gr).toString() + ", " + numberNotesInGroup;

            if (searchQuery.length == 1) {
                FindIdsByFrom(telegram_id, sqlSelect, sqlEnd, searchQuery[0], selectIdsFromDatabase, callback);
            } else {
                FindIdsByFromTo(telegram_id, sqlSelect, sqlEnd, searchQuery[0], searchQuery[1], selectIdsFromDatabase, callback);
            }
            break;
    }
}

function FindIdsByFromTo(telegram_id, sql, sqlEnd, from, to, func, callback) {
    let fromDate = new Date(from);
    let toDate = new Date(to);

    let sqlSelect = sql + " WHERE telegram_id=" + telegram_id.toString();

    fromDate.setHours(0, 0, 0, 0);
    toDate.setHours(23, 59, 59, 999);

    if (from === 0) {
        sqlSelect += " AND date_added <= " + fromDate.getUnixTime();
    } else {
        sqlSelect += " AND date_added >= " + fromDate.getUnixTime()
            + " AND date_added <= " + toDate.getUnixTime();
    }

    sqlSelect += " ORDER BY date_added ASC";
    sqlSelect += sqlEnd;

    func(sqlSelect, callback);
}

function FindIdsByFrom(telegram_id, sql, sqlEnd, from, func, callback) {
    let fromDate = new Date(from);
    fromDate.setHours(0, 0, 0, 0);

    let sqlSelect = sql + " WHERE telegram_id=" + telegram_id.toString()
        + " AND date_added >= " + fromDate.getUnixTime();
    sqlSelect += " ORDER BY date_added ASC";
    sqlSelect += sqlEnd;
    func(sqlSelect, callback);
}

function InsertIntoRTIndex(sql, values, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            callback(err, null);
            return;
        }

        connection.query(sql, values, function (err, results) {
            connection.release();
            if (err) {
                callback(err, null);
                return;
            }
            callback(null, results);
        });
    });
}


function addInsertedIdAndDataIntoRTIndex(data, callback) {
    let todayDate = new Date();
    let sqlInsert = 'INSERT INTO `' + table + '` (id, note, telegram_id, date_added) VALUES (?, ?, ?, ?)';
    let obj = [
        data.insertId,
        data.data.note,
        data.data.telegram_id,
        todayDate.getUnixTime()
    ];

    InsertIntoRTIndex(sqlInsert, obj, callback);
}

module.exports.NoteSphinxModel = NoteSphinxModel;
