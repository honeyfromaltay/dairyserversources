var Nconf = require('nconf');

Nconf.argv().env().file({file: './libs/config.json'});

module.exports.Nconf = Nconf;