let mysql = require('mysql');
let config = require('./config').Nconf;
let logger = require("./logger").logger;
let pool = mysql.createPool({
    connectionLimit: 100,
    host: config.get("hostDb"),
    user: config.get("user"),
    password: config.get("password"),
    database: config.get("database"),
    debug: false
});

pool.on('acquire', function (connection) {
    if (config.get("debug_mode") === true) {
        logger.log("Connection " + connection.threadId.toString() + " acquired");
    }
});

pool.on('release', function (connection) {
    if (config.get("debug_mode") === true) {
        logger.log("Connection " + connection.threadId.toString() + " released");
    }
});


let table = config.get("table");
let sessionTable = config.get("sessionTable");

function NoteModel(note) {
    this.telegram_id = note.telegram_id;
    this.note = note.note;
}

NoteModel.AddNoteIntoDatabase = InsertNoteIntoDatabase;
NoteModel.FindNotesByTelegramId = SelectAllNotesByTelegramIdFromDatabase;
NoteModel.FindLastMessageByNext = FindLastMessageByNext;
NoteModel.UpdateOrCreateSessionKeywords = UpdateOrCreateSessionKeywords;
NoteModel.FindLastNext = FindLastNext;
NoteModel.RegisterNewUser = registerNewUser;

function registerNewUser(telegram_id, login, callback) {
    let sql = "INSERT INTO users SET ?";
    let values = {telegram_id: telegram_id, login: login};
    InsertIntoDatabase(sql, values, callback);
}

function SelectLastNextDatabase(sql, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            callback(err, null);
            return;
        }
        connection.query(sql, [], function (err, rows) {
            connection.release();
            if (err) {
                callback(err, null);
                return
            }
            callback(null, rows)
        })
    })
}

function FindLastNext(telegram_id, callback) {
    let sql = "SELECT searchQuery, groupNumber, command FROM " + sessionTable
        + " WHERE telegram_id = " + telegram_id.toString();
    SelectLastNextDatabase(sql, callback)
}

function UpdateSessionDatabase(sqlUpdate, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            callback(err, null);
            return;
        }
        connection.query(sqlUpdate, [], function (err, rows) {
            connection.release();
            if (err) {
                callback(err, null);
                return
            }
            callback(null, rows);
        });
    })
}

function InsertIntoDatabase(sqlInsert, values, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            callback(err, null);
            return;
        }
        connection.query(sqlInsert, [values], function (err, rows) {
            connection.release();
            if (err) {
                callback(err, null);
                return
            }
            callback(null, rows);
        });
    })
}

function insertIntoDatabase(sql, values, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            callback(err, null);
            return;
        }
        connection.query(sql, [values], function (err, rows) {
            connection.release();
            if (err) {
                callback(err, null);
                return
            }
            callback(null, rows);
        });
    })
}

function UpdateOrCreateSessionKeywords(telegram_id, searchQuery, command, groupNumber, callback) {
    let sqlUpdate = "UPDATE " + config.get("sessionTable") + " SET searchQuery = '" + searchQuery
        + "' " +
        ", groupNumber = '" + groupNumber + "', command = '" + command + "' WHERE telegram_id = '" + telegram_id.toString() + "'";
    UpdateSessionDatabase(sqlUpdate, function (err, rows) {
        if (err) {
            callback(err, null);
            return
        }
        if (rows.affectedRows == 0) {
            let sqlInsert = "INSERT INTO " + config.get("sessionTable") + " (telegram_id, searchQuery, " +
                "groupNumber) VALUES (?)";
            InsertIntoDatabase(sqlInsert, [telegram_id.toString(), searchQuery, groupNumber], callback);
        } else {
            callback(null, rows);
        }
    });
}

function InsertNoteIntoDatabase(note, callback) {
    let sqlInsert = "INSERT INTO " + table + " (telegram_id, note) VALUES (?)";
    insertIntoDatabase(sqlInsert, [note.telegram_id.toString(), note.note], callback);
}

function selectFromDatabase(sql, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            logger.log(err);
            return;
        }
        var query = connection.query(sql, function (err, rows) {
            if (err) {
                logger.log(err);
                return
            }
            connection.release();

            callback(null, rows);
        });
    });
}

function SelectAllNotesByTelegramIdFromDatabase(telegram_id, ids, callback) {
    let sqlSelect = "SELECT * FROM " + table + " WHERE telegram_id=" + telegram_id.toString()
        + " AND id IN('" + ids.join("','") + "') ORDER BY date_added ASC";
    selectFromDatabase(sqlSelect, callback);
}

function FindLastMessageByNext(telegram_id, callback) {
    let sqlSelect = "SELECT * FROM " + sessionTable + " WHERE telegram_id="
        + telegram_id.toString();
    selectFromDatabase(sqlSelect, callback);
}

module.exports.NoteModel = NoteModel;
