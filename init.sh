#!/bin/bash
export APP_ENV="dev";
export TELEGRAM_TOKEN="478506557:AAH6bxV-7Nvcj-4bmUSIsDS7I2ETvsRQ_ZQ";

docker-compose rm -s -v -f db
docker-compose rm -s -v -f sphinx
docker-compose rm -s -v -f telegrambot

docker-compose build

docker-compose up -d db
sleep 15;
cat ./db/command/telegramBot.sql | docker exec -i $(docker-compose ps -q db) mysql -u root --password="dev"
cat ./db/command/testData.sql | docker exec -i $(docker-compose ps -q db) mysql -u root --password="dev"

docker-compose up -d sphinx
sleep 15;
echo 'mysql -h0 -P9306 -e "TRUNCATE RTINDEX notes_rt;"' | docker exec -i $(docker-compose ps -q sphinx) sh
echo 'mysql -h0 -P9306 -e "ATTACH INDEX notes TO RTINDEX notes_rt;"' | docker exec -i $(docker-compose ps -q sphinx) sh

docker-compose stop sphinx
docker-compose stop db

docker-compose ps
