-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 28 2017 г., 20:41
-- Версия сервера: 5.7.19-0ubuntu0.16.04.1
-- Версия PHP: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `telegramBot`
--

DROP DATABASE `telegramBot`;

CREATE DATABASE IF NOT EXISTS `telegramBot`
  DEFAULT CHARACTER SET utf8mb4
  COLLATE utf8mb4_general_ci;
USE `telegramBot`;

-- --------------------------------------------------------

--
-- Структура таблицы `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes` (
  `id`          INT(10) UNSIGNED NOT NULL,
  `telegram_id` BIGINT(20)       NOT NULL,
  `note`        TEXT             NOT NULL,
  `date_added`  DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `id`          INT(11)    NOT NULL,
  `telegram_id` BIGINT(20) NOT NULL,
  `searchQuery` TEXT,
  `groupNumber` BIGINT(20) DEFAULT NULL,
  `command`     TEXT
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id`          BIGINT PRIMARY KEY AUTO_INCREMENT,
  `telegram_id` BIGINT,
  `login`       TEXT
);

CREATE UNIQUE INDEX `users_id_uindex`
  ON `users` (`id`);

CREATE UNIQUE INDEX `users_telegram_id_uindex`
  ON `users` (`telegram_id`);

--
-- Индексы таблицы `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для таблицы `notes`
--
ALTER TABLE `notes`
  MODIFY `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 1;
--
-- AUTO_INCREMENT для таблицы `session`
--
ALTER TABLE `session`
  MODIFY `id` INT(11) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 1;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;

--
-- Создаём пользователя для разработки
--
DROP USER IF EXISTS `dev`@`localhost`;

CREATE USER `dev`@`localhost`
  IDENTIFIED BY 'dev';
GRANT ALL ON `notes` TO `dev`@`localhost`;
GRANT ALL ON `session` TO `dev`@`localhost`;
GRANT ALL ON `users` TO `dev`@`localhost`;