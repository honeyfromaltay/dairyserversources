#!/usr/bin/env bash

#Остоновим pm2 для того, чтобы подготовить тестовое окружение.
#Удалим старые данные.
#pm2 stop index
#sudo rm -R /var/www/node/dairyserversources/
#sudo mkdir /var/www/node/dairyserversources/
#sudo cp -r ../src/* /var/www/node/dairyserversources/

echo "#1 sector is done"

#Скопируем новый файл config.json
#sudo cp ~/config.json /var/www/node/dairyserversources/libs/

echo "#2 sector is done"

#Сектор Mysql удаления данных и заполнения новых.
mysql -u dev -p < ./command/telegramBot.sql
mysql -u dev -p < ./command/testData.sql

echo "#3 sector is done"

#Сектор sphinx индексирования
#searchd --stop
#/etc/init.d/sphinxsearch stop
#cp /etc/sphinxsearch/sphinx.conf ~/sphinx.conf.last
#sudo cp sphinx.conf /etc/sphinxsearch/sphinx.conf
#sleep 2
#/etc/init.d/sphinxsearch start
#indexer --rotate --all
#mysql -h0 -P9306 -e "TRUNCATE RTINDEX notes_rt;"
#mysql -h0 -P9306 -e "ATTACH INDEX notes TO RTINDEX notes_rt;"

echo "#4 sector is done"

#Запускаем pm2
#cd /var/www/node/dairyserversources
#sudo npm update
#pm2 start index

echo "#5 sector is done"


#Запускаем Unit тесты

echo "#6 sector is done"
